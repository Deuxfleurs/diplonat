Diplonat
========

[![status-badge](https://woodpecker.deuxfleurs.fr/api/badges/40/status.svg)](https://woodpecker.deuxfleurs.fr/repos/40)

## Feature set

Diplonat performs two main tasks:

1) ensure that all services are accessible from the Internet
   - it detects services by watching Consul and looking for a special "diplonat" tag (see below)
   - for each service, it configures the host firewall with iptables and the router NAT with IGD
2) autodiscovery of the public IP addresses of the local node
   - it uses STUN to a remote server for IPv4, and looks locally for a usable IPv6 address
   - it then writes the discovered IP addresses to Consul, so that other services can use them (D53 for the DNS, Garage, Jitsi...)

## Understand scope

 * Reconfigure __local__ environment when provisionning a __cluster__ service
   * Reconfigure host on demand according to service needs (Firewall)
   * Reconfigure host local network according to service needs (Router NAT)
 * Operate a global reconfiguration that associate the tuple (__local__ environment information, a __cluster__ service)
   * Reconfigure an external service with local info (DNS with public IP returned by the router via IGD)

## Dependencies

The `reqwest` crate "will make use of system-native transport layer security to connect to HTTPS destinations". See [`reqwest`'s documentation](https://docs.rs/reqwest/0.9.18/reqwest/#tls) for more information. 


## Operate

You need to add the following to your nomad config file :

```
client {
  [...]

  options {
    docker.privileged.enabled = "true"
  } 
}
```


```bash
cargo build
consul agent -dev # in a separate terminal

# adapt following values to your configuration
export DIPLONAT_PRIVATE_IP="192.168.0.18"
export DIPLONAT_REFRESH_TIME="60"
export DIPLONAT_EXPIRATION_TIME="300"
export DIPLONAT_CONSUL_NODE_NAME="lheureduthe"
export RUST_LOG=debug
cargo run
```

## Contributing

Refer to [CONTRIBUTING.md](./CONTRIBUTING.md).

## Design Guidelines

Diplonat is made of a set of Components.
Components communicate between them thanks to [tokio::sync::watch](https://docs.rs/tokio/0.2.21/tokio/sync/index.html#watch-channel) transferring copiable messages.
Each message must contain the whole state (and not a transition) as messages can be lost if a more recent message is received.
This choice has been made to limit bugs.
If you need to watch two actors and merge their content, you may use [tokio::sync::select](https://docs.rs/tokio/0.2.21/tokio/macro.select.html).
When you read a value from source 1, you must cache it to be able to merge it later when you read from source 2.

## About Consul Catalog

  * We query the `/v1/catalog/node/<node>` endpoint
  * We can watch it thanks to [Blocking Queries](https://www.consul.io/api/features/blocking.html)

eg:

```bash
curl -vvv http://127.0.0.1:8500/v1/catalog/node/lheureduthe
# returns X-Consul-Index: 15
curl -vvv http://127.0.0.1:8500/v1/catalog/node/lheureduthe?index=15
```

Each time you do the request, the whole list of services bound to the node is returned.


To test the Consul Catalog part, you can do:

```bash
consul agent -dev #in a separate terminal, if not already running
consul services register -name=fake_leet -tag="(diplonat (tcp_port 1337) (tcp_port 1338 1339))"
consul services register -name=fake_dns  -tag="(diplonat (udp_port 53) (tcp_port 53))"
consul services register -name=fake_irc  -tag="(diplonat (udp_port 6667 6666))"
consul services -id=example
```

## License

This software is published under the AGPLv3 license.

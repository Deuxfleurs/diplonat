mod autodiscovery_actor;
mod config;
mod consul;
mod consul_actor;
mod diplonat;
mod fw;
mod fw_actor;
mod igd_actor;
mod messages;

use diplonat::Diplonat;
use log::*;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    info!("Starting Diplonat");

    Diplonat::new()
        .await
        .expect("Setup failed")
        .listen()
        .await
        .expect("A runtime error occured");
}

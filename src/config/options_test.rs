use std::{collections::HashMap, time::Duration};

use crate::config::*;

// Environment variables are set for the entire process and
// tests are run whithin the same process.
// => We cannot test ConfigOpts::from_env(),
// because tests modify each other's environment.
// This is why we only test ConfigOpts::from_iter(iter).

fn minimal_valid_options() -> HashMap<String, String> {
    let mut opts = HashMap::new();
    opts.insert(
        "DIPLONAT_CONSUL_NODE_NAME".to_string(),
        "consul_node".to_string(),
    );
    opts
}

fn all_valid_options() -> HashMap<String, String> {
    let mut opts = minimal_valid_options();
    opts.insert("DIPLONAT_EXPIRATION_TIME".to_string(), "30".to_string());
    opts.insert(
        "DIPLONAT_STUN_SERVER".to_string(),
        "stun.nextcloud.com:443".to_string(),
    );
    opts.insert(
        "DIPLONAT_PRIVATE_IP".to_string(),
        "172.123.43.55".to_string(),
    );
    opts.insert("DIPLONAT_REFRESH_TIME".to_string(), "10".to_string());
    opts.insert(
        "DIPLONAT_CONSUL_URL".to_string(),
        "http://127.0.0.1:9999".to_string(),
    );
    opts.insert("DIPLONAT_ACME_ENABLE".to_string(), "true".to_string());
    opts.insert(
        "DIPLONAT_ACME_EMAIL".to_string(),
        "bozo@bozo.net".to_string(),
    );
    opts
}

#[test]
#[should_panic]
fn err_empty_env() {
    std::env::remove_var("DIPLONAT_CONSUL_NODE_NAME");
    ConfigOpts::from_env().unwrap();
}

#[test]
fn ok_from_iter_minimal_valid_options() {
    let opts = minimal_valid_options();
    let rt_config = ConfigOpts::from_iter(opts.clone()).unwrap();

    assert_eq!(
        &rt_config.consul.node_name,
        opts.get(&"DIPLONAT_CONSUL_NODE_NAME".to_string()).unwrap()
    );
    assert_eq!(rt_config.consul.url, CONSUL_URL.to_string());
    assert_eq!(
        rt_config.firewall.refresh_time,
        Duration::from_secs(REFRESH_TIME.into())
    );
    let igd = rt_config.igd.unwrap();
    assert!(igd.private_ip.is_none());
    assert_eq!(
        igd.expiration_time,
        Duration::from_secs(EXPIRATION_TIME.into())
    );
    assert_eq!(igd.refresh_time, Duration::from_secs(REFRESH_TIME.into()));
}

#[test]
#[should_panic]
fn err_from_iter_invalid_refresh_time() {
    let mut opts = minimal_valid_options();
    opts.insert("DIPLONAT_EXPIRATION_TIME".to_string(), "60".to_string());
    opts.insert("DIPLONAT_REFRESH_TIME".to_string(), "60".to_string());
    ConfigOpts::from_iter(opts).unwrap();
}

#[test]
fn ok_from_iter_all_valid_options() {
    let opts = all_valid_options();
    let rt_config = ConfigOpts::from_iter(opts.clone()).unwrap();

    let expiration_time = Duration::from_secs(
        opts.get(&"DIPLONAT_EXPIRATION_TIME".to_string())
            .unwrap()
            .parse::<u64>()
            .unwrap()
            .into(),
    );
    let refresh_time = Duration::from_secs(
        opts.get(&"DIPLONAT_REFRESH_TIME".to_string())
            .unwrap()
            .parse::<u64>()
            .unwrap()
            .into(),
    );

    assert_eq!(
        &rt_config.consul.node_name,
        opts.get(&"DIPLONAT_CONSUL_NODE_NAME".to_string()).unwrap()
    );
    assert_eq!(
        &rt_config.consul.url,
        opts.get(&"DIPLONAT_CONSUL_URL".to_string()).unwrap()
    );
    assert_eq!(rt_config.firewall.refresh_time, refresh_time);
    let igd = rt_config.igd.unwrap();
    assert_eq!(
        &igd.private_ip.unwrap().to_string(),
        opts.get(&"DIPLONAT_PRIVATE_IP".to_string()).unwrap()
    );
    assert_eq!(igd.expiration_time, expiration_time);
    assert_eq!(igd.refresh_time, refresh_time);
}

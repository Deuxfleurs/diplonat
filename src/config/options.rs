use anyhow::Result;
use serde::Deserialize;

use crate::config::RuntimeConfig;

// This code is inspired by the Trunk crate (https://github.com/thedodd/trunk)

// This file parses the options that can be declared in the environment.
// runtime.rs applies business logic and builds RuntimeConfig structs.

/// Base configuration options
#[derive(Clone, Default, Deserialize)]
pub struct ConfigOptsBase {
    /// This node's private IP address [default: None]
    pub private_ip: Option<String>,
    /// Expiration time for IGD rules [default: 60]
    pub expiration_time: Option<u16>,
    /// Refresh time for IGD and Firewall rules [default: 300]
    pub refresh_time: Option<u16>,
    /// STUN server [default: stun.nextcloud.com:443]
    pub stun_server: Option<String>,
    /// IPv6-only mode (disables IGD, IPv4 firewall and IPv4 address autodiscovery) [default: false]
    #[serde(default)]
    pub ipv6_only: bool,
}

/// Consul configuration options
#[derive(Clone, Default, Deserialize)]
pub struct ConfigOptsConsul {
    /// Consul's node name [default: None]
    pub node_name: Option<String>,
    /// Consul's REST URL [default: "http://127.0.0.1:8500"]
    pub url: Option<String>,
    /// Consul's CA certificate [default: None]
    pub ca_cert: Option<String>,
    /// Skip TLS verification for Consul server [default: false]
    #[serde(default)]
    pub tls_skip_verify: bool,
    /// Consul's client certificate [default: None]
    pub client_cert: Option<String>,
    /// Consul's client key [default: None]
    pub client_key: Option<String>,
}

/// Model of all potential configuration options
pub struct ConfigOpts {
    pub base: ConfigOptsBase,
    pub consul: ConfigOptsConsul,
}

impl ConfigOpts {
    pub fn from_env() -> Result<RuntimeConfig> {
        let base: ConfigOptsBase = envy::prefixed("DIPLONAT_").from_env()?;
        let consul: ConfigOptsConsul = envy::prefixed("DIPLONAT_CONSUL_").from_env()?;

        RuntimeConfig::new(Self {
            base: base,
            consul: consul,
        })
    }

    // Currently only used in tests
    #[cfg(test)]
    pub fn from_iter<Iter: Clone>(iter: Iter) -> Result<RuntimeConfig>
    where
        Iter: IntoIterator<Item = (String, String)>,
    {
        let base: ConfigOptsBase = envy::prefixed("DIPLONAT_").from_iter(iter.clone())?;
        let consul: ConfigOptsConsul =
            envy::prefixed("DIPLONAT_CONSUL_").from_iter(iter.clone())?;

        RuntimeConfig::new(Self {
            base: base,
            consul: consul,
        })
    }
}

mod options;
#[cfg(test)]
mod options_test;
mod runtime;

pub use options::{ConfigOpts, ConfigOptsBase, ConfigOptsConsul};
pub use runtime::{
    RuntimeConfig, RuntimeConfigAutoDiscovery, RuntimeConfigConsul, RuntimeConfigFirewall,
    RuntimeConfigIgd,
};

pub const EXPIRATION_TIME: u16 = 300;
pub const REFRESH_TIME: u16 = 60;
pub const CONSUL_URL: &str = "http://127.0.0.1:8500";
pub const STUN_SERVER: &str = "stun.nextcloud.com:443";

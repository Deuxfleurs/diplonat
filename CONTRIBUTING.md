# Contributing to Diplonat 

## Development guidelines

### Code formatting 

[Our CI pipeline](./.drone.yml) features a verification of the code format, using [rustfmt](https://github.com/rust-lang/rustfmt).

#### Installing rustfmt

You must install a very recent version of `rustfmt` through rust nightly

To install: 

```
rustup toolchain install nightly-x86_64-unknown-linux-gnu
rustup component add rustfmt --toolchain nightly
```

#### Usage 

To run on Diplonat, launch the following in the root directory:

```
cargo +nightly fmt
``` 

This will format the whole repository using the settigs defined in [`.rustfmt.toml`](./.rustfmt.toml).

#### Auto-format code

You can automate formatting in a number of ways:

[Setup your IDE to use `rustfmt`](https://github.com/rust-lang/rustfmt#running-rustfmt-from-your-editor).

Setup a git hook to run `rustfmt` before each commit:

```bash
cat <<EOF > .git/hooks/pre-commit
#!/bin/bash

cargo +nightly fmt
EOF
chmod +x .git/hooks/pre-commit
```

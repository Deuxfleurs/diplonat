FROM rust:1.69-bullseye as builder

RUN apt-get update && \
    apt-get install -y libssl-dev pkg-config

WORKDIR /srv

# Build dependencies and cache them
COPY Cargo.* ./
RUN mkdir -p src && \
    echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs && \
    cargo build --release && \
    rm -r src && \
    rm target/release/deps/diplonat*

# Build final app
COPY ./src ./src
RUN cargo build --release

FROM debian:bullseye-slim
RUN apt-get update && apt-get install -y libssl1.1 iptables
COPY --from=builder /srv/target/release/diplonat /usr/local/sbin/diplonat
CMD ["/usr/local/sbin/diplonat"]
